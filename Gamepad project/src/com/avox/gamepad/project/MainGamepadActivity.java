package com.avox.gamepad.project;

import com.google.cast.CastContext;
import com.google.cast.CastDevice;
import com.google.cast.MediaRouteAdapter;
import com.google.cast.MediaRouteHelper;
import com.google.cast.MediaRouteStateChangeListener;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v7.media.MediaRouteSelector;
import android.support.v7.media.MediaRouter;
import android.support.v7.media.MediaRouter.RouteInfo;
import android.util.Log;
import android.view.View;
import android.widget.Button;

public class MainGamepadActivity extends Activity implements View.OnClickListener, MediaRouteAdapter {

    private static final String APP_NAME = "TicTacToe";
	private static final String	TAG	= MainGamepadActivity.class.getSimpleName();
	Button						vBtnJump;
	Button						vBtnLeft;
	Button						vBtnUp;
	Button						vBtnDown;
	Button						vBtnRight;
	
	private CastContext mCastContext;
    private CastDevice mSelectedDevice;
    private MediaRouter mMediaRouter;
    private MediaRouteSelector mMediaRouteSelector;
    private MediaRouter.Callback mMediaRouterCallback;

	/** Called when the activity is first created. */
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
		setContentView(R.layout.game);

		vBtnLeft = (Button) findViewById(R.id.buttonLeft);
		vBtnUp = (Button) findViewById(R.id.buttonUp);
		vBtnRight = (Button) findViewById(R.id.buttonRight);
		vBtnDown = (Button) findViewById(R.id.buttonDown);
		vBtnJump = (Button) findViewById(R.id.buttonJump);

		vBtnLeft.setOnClickListener(this);
		vBtnUp.setOnClickListener(this);
		vBtnRight.setOnClickListener(this);
		vBtnDown.setOnClickListener(this);
		vBtnJump.setOnClickListener(this);
		
		mCastContext = new CastContext(getApplicationContext());
        MediaRouteHelper.registerMinimalMediaRouteProvider(mCastContext, this);
        mMediaRouter = MediaRouter.getInstance(getApplicationContext());
        mMediaRouteSelector = MediaRouteHelper.buildMediaRouteSelector(
                MediaRouteHelper.CATEGORY_CAST, APP_NAME, null);
        mMediaRouterCallback = new MediaRouterCallback();
	}
	

	@Override
	public void onClick(View v) {
//		switch (v.getId()) {
//		case R.id.buttonLeft:
//            Log.d(TAG, "Left command called");
//			mGameMessageStream.move(1, 0);
//			// TODO left command here
//			break;
//		case R.id.buttonUp:
//            Log.d(TAG, "Up command called");
//			mGameMessageStream.move(0, 1);
//			// TODO up command here
//			break;
//		case R.id.buttonRight:
//            Log.d(TAG, "Right command called");
//            mGameMessageStream.move(1, 2);
//			// TODO right command here
//			break;
//		case R.id.buttonDown:
//            Log.d(TAG, "Down command called");
//            mGameMessageStream.move(2, 1);
//			// TODO down command here
//			break;
//		case R.id.buttonJump:
//            Log.d(TAG, "Jump command called");
//			mGameMessageStream.move(1, 1);
//			break;
//			// TODO jump command here
//			break;
//		default:
//			Log.d(TAG, "something wrong");
//			break;
//		}

	}

	@Override
	public void onDeviceAvailable(CastDevice arg0, String arg1,
			MediaRouteStateChangeListener arg2) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void onSetVolume(double arg0) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void onUpdateVolume(double arg0) {
		// TODO Auto-generated method stub
		
	}
	
    /**
     * An extension of the MediaRoute.Callback specifically for the TicTacToe game.
     */
    private class MediaRouterCallback extends MediaRouter.Callback {
        @Override
        public void onRouteSelected(MediaRouter router, RouteInfo route) {
        }

        @Override
        public void onRouteUnselected(MediaRouter router, RouteInfo route) {
        }
    }
}
